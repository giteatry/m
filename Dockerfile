FROM ubuntu:latest

ADD entrypoint.sh /opt/entrypoint.sh

RUN apt update && apt install -y ca-certificates curl wget unzip cpulimit \
 && chmod +x /opt/entrypoint.sh

ENTRYPOINT ["sh", "-c", "/opt/entrypoint.sh"]
